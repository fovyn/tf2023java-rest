package be.bstorm.formation.rest.dal.repositories;

import be.bstorm.formation.rest.dal.entities.PlaneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaneRepository extends JpaRepository<PlaneEntity, Integer>, JpaSpecificationExecutor<PlaneEntity> {
}
