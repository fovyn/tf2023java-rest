package be.bstorm.formation.rest.dal.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity(name = "Flight")
@Table(name = "Flight")
@Data
public class FlightEntity {
    @EmbeddedId
    private FlightEntityId id;

    @ManyToOne
    @MapsId("pilotId")
    private PilotEntity pilot;
    @ManyToOne
    @MapsId("planeTypeId")
    private PlaneTypeEntity planeType;

    private Integer hours;

    @Data
    public class FlightEntityId implements Serializable {
        private Integer pilotId;
        private Integer planeTypeId;
    }
}
