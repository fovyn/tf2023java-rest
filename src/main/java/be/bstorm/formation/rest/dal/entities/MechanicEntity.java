package be.bstorm.formation.rest.dal.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import java.util.List;

@Entity(name = "Mechanic")
@Table(name = "Mechanic")
public class MechanicEntity extends MoralEntity {

    @ManyToMany(targetEntity = PlaneTypeEntity.class)
    private List<PlaneTypeEntity> habilities;
}
