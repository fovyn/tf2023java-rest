package be.bstorm.formation.rest.dal.repositories;

import be.bstorm.formation.rest.dal.entities.FlightEntity;
import be.bstorm.formation.rest.dal.entities.PilotEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, Integer>, JpaSpecificationExecutor<FlightEntity> {

    @Query(value = "SELECT f FROM Flight f JOIN f.pilot p WHERE p = :pilot")
    List<FlightEntity> findAllByPilot(@Param("pilot")PilotEntity pilot);
    @Query(value = "SELECT f FROM Flight f JOIN f.pilot p WHERE p.id = :pilot")
    List<FlightEntity> findAllByPilotId(@Param("pilot")Integer pilotId);
}
