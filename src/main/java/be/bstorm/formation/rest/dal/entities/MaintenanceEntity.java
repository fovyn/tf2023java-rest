package be.bstorm.formation.rest.dal.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity(name = "Maintenance")
@Table(name = "Maintenance")
@Data
public class MaintenanceEntity extends BaseEntity<Long> {

    private String number;
    private String object;
    private LocalTime duration;

    @ManyToOne
    private PlaneEntity plane;
    @ManyToOne
    private MechanicEntity worker;
    @ManyToOne
    private MechanicEntity checker;

    private LocalDate date;
}
