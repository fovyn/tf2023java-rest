package be.bstorm.formation.rest.dal.repositories;

import be.bstorm.formation.rest.dal.entities.MechanicEntity;
import be.bstorm.formation.rest.dal.entities.PilotEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MechanicRepository extends JpaRepository<MechanicEntity, Integer>, JpaSpecificationExecutor<MechanicEntity> {
}
