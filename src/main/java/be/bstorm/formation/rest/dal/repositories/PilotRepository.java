package be.bstorm.formation.rest.dal.repositories;

import be.bstorm.formation.rest.dal.entities.MechanicEntity;
import be.bstorm.formation.rest.dal.entities.PilotEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PilotRepository extends JpaRepository<PilotEntity, Integer>, JpaSpecificationExecutor<PilotEntity> {

//    Optional<PilotEntity> findOneByLicence(String licence);
}
