package be.bstorm.formation.rest.dal.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity(name = "Pilot")
@Table(name= "pilot")
@Data
public class PilotEntity extends MoralEntity {
    private String licence;
}
