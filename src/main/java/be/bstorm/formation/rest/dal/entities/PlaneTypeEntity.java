package be.bstorm.formation.rest.dal.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity(name = "PlaneType")
@Table(name = "PlaneType")
@Data
public class PlaneTypeEntity extends BaseEntity<Integer> {
    private String name;
    private String constructor;
    private String power;
    private String nbPlace;
}
