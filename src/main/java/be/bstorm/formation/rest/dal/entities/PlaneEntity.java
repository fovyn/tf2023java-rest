package be.bstorm.formation.rest.dal.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Entity(name= "Plane")
@Table(name= "Plane")
@Data
public class PlaneEntity extends BaseEntity<Integer> implements Serializable {
    private String imma;

    @ManyToOne(targetEntity = MoralEntity.class)
    private MoralEntity owner;
    private LocalDate buyDate;

    @ManyToOne(targetEntity = PlaneTypeEntity.class)
    private PlaneTypeEntity type;
}
