package be.bstorm.formation.rest.dal.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.LocalDate;

@MappedSuperclass
@EntityListeners(value = { AuditingEntityListener.class })
@Data
public abstract class BaseEntity<TKey extends Number> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private TKey id;

    @CreatedDate
    private LocalDate createdAt;
    @LastModifiedDate
    private LocalDate updatedAt;
    private boolean active = true;
}
