package be.bstorm.formation.rest.dal.entities;

import be.bstorm.formation.rest.dal.entities.embedded.Address;
import jakarta.persistence.*;
import lombok.Data;

@Entity(name = "Moral")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public class MoralEntity extends BaseEntity<Integer> {
    private String name;
    @Embedded
    private Address address;
    private String phone;
}
