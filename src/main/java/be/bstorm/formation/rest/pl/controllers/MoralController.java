package be.bstorm.formation.rest.pl.controllers;

import be.bstorm.formation.rest.bll.moral.MoralService;
import be.bstorm.formation.rest.dal.entities.MoralEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = {"/morals"})
public class MoralController {
    private final MoralService moralService;

    public MoralController(MoralService moralService) {
        this.moralService = moralService;
    }

    @GetMapping
    public ResponseEntity<List<MoralEntity>> getAllAction() {
        return ResponseEntity.ok(this.moralService.findAll().toList());
    }
}
