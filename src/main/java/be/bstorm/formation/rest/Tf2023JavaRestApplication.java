package be.bstorm.formation.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tf2023JavaRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(Tf2023JavaRestApplication.class, args);
    }

}
