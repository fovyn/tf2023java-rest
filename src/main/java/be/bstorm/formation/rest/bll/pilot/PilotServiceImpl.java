package be.bstorm.formation.rest.bll.pilot;

import be.bstorm.formation.rest.dal.entities.PilotEntity;
import be.bstorm.formation.rest.dal.entities.PilotEntity_;
import be.bstorm.formation.rest.dal.repositories.PilotRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class PilotServiceImpl implements PilotService {
    private final PilotRepository pilotRepository;

    public PilotServiceImpl(PilotRepository pilotRepository) {
        this.pilotRepository = pilotRepository;
    }

    @Override
    public Collection<PilotEntity> getAll(int page, int size) {
        return this.pilotRepository.findAll(PageRequest.of(page, size)).stream().toList();
    }
    @Override
    public Optional<PilotEntity> getOneById(int id) {
        Specification<PilotEntity> specification = ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(PilotEntity_.ID), id));

        return this.findBySpec(specification);
    }
    @Override
    public Optional<PilotEntity> getOneByLicence(String licence) {
        Specification<PilotEntity> licenceSpec = (root, query, cb) -> cb.equal(root.get(PilotEntity_.LICENCE), licence);
        return this.findBySpec(licenceSpec);
    }
    private Optional<PilotEntity> findBySpec(Specification<PilotEntity> specification) {
        return this.pilotRepository.findBy(specification, FluentQuery.FetchableFluentQuery::first);
    }
    private Stream<PilotEntity> findAllBySpec(Specification<PilotEntity> specification) {
        return this.pilotRepository.findBy(specification, FluentQuery.FetchableFluentQuery::stream);
    }
}
