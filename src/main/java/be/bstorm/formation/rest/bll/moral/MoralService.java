package be.bstorm.formation.rest.bll.moral;

import be.bstorm.formation.rest.dal.entities.MoralEntity;

import java.util.stream.Stream;

public interface MoralService {
    Stream<MoralEntity> findAll();
}
