package be.bstorm.formation.rest.bll.pilot;

import be.bstorm.formation.rest.dal.entities.PilotEntity;

import java.util.Collection;
import java.util.Optional;

public interface PilotService {
    Collection<PilotEntity> getAll(int page, int size);
    Optional<PilotEntity> getOneById(int id);
    Optional<PilotEntity> getOneByLicence(String licence);
}
