package be.bstorm.formation.rest.bll.moral;

import be.bstorm.formation.rest.dal.entities.MoralEntity;
import be.bstorm.formation.rest.dal.repositories.MoralRepository;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class MoralServiceImpl implements MoralService {
    private final MoralRepository moralRepository;

    public MoralServiceImpl(MoralRepository moralRepository) {
        this.moralRepository = moralRepository;
    }

    public Stream<MoralEntity> findAll() {
        return this.moralRepository.findAll().stream();
    }
}
